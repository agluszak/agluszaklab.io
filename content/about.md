+++
title = "About"
date = 2021-12-12

[taxonomies]
+++

My name is Andrzej Głuszak.
I'm a bachelor of Computer Science at the University of Warsaw interested in programming language development and static code analysis.
I'm working as a teaching assistant at the Faculty of Mathematics, Informatics and Mechanics, but I'm also studying Creative Writing.

<!-- more -->